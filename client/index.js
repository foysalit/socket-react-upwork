const shell = require('electron').shell;
const io = require('socket.io-client');

const socket = io('http://localhost:3000', {
	transports: [ 'websocket' ]
});

socket.on('url', (data) => {
	shell.openExternal(data.url);
});

document.querySelector('#request_url').addEventListener('click', () => {
	socket.emit('url.request');
});