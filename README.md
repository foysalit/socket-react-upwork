# How to run

- Make sure you have electron.js installed globally and accessible through `electron --version` command for instance.
- Make sure you run `npm install` in both client and server directory.
- Run the server from `/server` directory using `node index.js`.
- Run the client from `/client` directory using `electron .`. 