var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.get('/', function(req, res){
	res.json({success: true});
});

const sendUrl = (socket) => {
	const t = new Date(),
		url = 'http://google.com';

	socket.emit('url', {url, t});
};

io.on('connection', function(socket) {
  	socket.on('url.request', sendUrl.bind(null, socket));

  	setTimeout(() => sendUrl(socket), 2000);
});

http.listen(port, function(){
	console.log('listening on *:' + port);
});